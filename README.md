# Terry's Personal Dotfiles

## Requirements

- Install [neovim](https://github.com/neovim/neovim/wiki/Installing-Neovim) as the text editor.
- Install [Hermit](https://pcaro.es/p/hermit/) as the font.
- Use [Solarized Dark](https://ethanschoonover.com/solarized/) as the color scheme of the terminal.
- Install [vim-plug](https://github.com/junegunn/vim-plug) as the plugin manager for vim.

## Setup

```
sudo apt install -y build-essential cmake python-dev python3-dev
git clone https://gitlab.com/terenceponce/dotfiles
cp gitconfig ~/.gitconfig
cp gitignore_global ~/.gitignore_global
cp vimrc ~/.config/nvim/init.vim
```

Afterwards, open `nvim` and do `:PlugInstall`
